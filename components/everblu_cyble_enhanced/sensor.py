import esphome.codegen as cg
import esphome.config_validation as cv
from esphome.components import sensor
from esphome.const import (
    CONF_ID,
    CONF_INDEX,
    CONF_FREQUENCY,
    DEVICE_CLASS_WATER,
    STATE_CLASS_TOTAL_INCREASING,
    UNIT_PERCENT,
    STATE_CLASS_MEASUREMENT
)

CONF_YEAR = "year"
CONF_SERIAL = "serial"

everblu_cyble_enhanced_ns = cg.esphome_ns.namespace("everblu_cyble_enhanced")


EverbluCybleEnhancedComponent = everblu_cyble_enhanced_ns.class_(
    "EverbluCybleEnhancedComponent", cg.PollingComponent
)

CONFIG_SCHEMA = (
    cv.Schema(
        {
            cv.GenerateID(): cv.declare_id(EverbluCybleEnhancedComponent),
            cv.Required(CONF_FREQUENCY): cv.float_,
            cv.Required(CONF_SERIAL): cv.int_,
            cv.Required(CONF_YEAR): cv.int_,
            cv.Optional(CONF_INDEX): sensor.sensor_schema(
                unit_of_measurement="L",
                accuracy_decimals=1,
                device_class=DEVICE_CLASS_WATER,
                state_class=STATE_CLASS_TOTAL_INCREASING,
            ),
        }
    )
    .extend(cv.polling_component_schema("3h"))
)


async def to_code(config):
    var = cg.new_Pvariable(config[CONF_ID])
    await cg.register_component(var, config)

    cg.add(var.set_frequency(config[CONF_FREQUENCY]))
    cg.add(var.set_year(config[CONF_YEAR]))
    cg.add(var.set_serial(config[CONF_SERIAL]))

    if index_config := config.get(CONF_INDEX):
        sens = await sensor.new_sensor(index_config)
        cg.add(var.set_index_sensor(sens))