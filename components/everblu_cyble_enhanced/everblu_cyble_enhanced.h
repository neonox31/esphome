#pragma once

#include "esphome/core/component.h"
#include "esphome/components/sensor/sensor.h"

namespace esphome {
namespace everblu_cyble_enhanced {

class EverbluCybleEnhancedComponent : public PollingComponent {
public:
    void set_frequency(float frequency) {frequency_ = frequency;};
    void set_year(int year) {year_ = year;};
    void set_serial(int serial) {serial_ = serial;};
    void set_index_sensor(sensor::Sensor *index_sensor) {index_sensor_ = index_sensor;};
    void setup() override;
    void dump_config() override;
    float get_setup_priority() const override;
    void update() override;
protected:
    float          frequency_;
    int            year_;
    int            serial_;
    sensor::Sensor *index_sensor_;
};

}  // namespace everblu_cyble_enhanced
}  // namespace esphome