#include "everblu_cyble_enhanced.h"
#include "esphome/core/hal.h"
#include "esphome/core/log.h"
#include "cc1101.h"
#include "everblu_meters.h"

namespace esphome {
namespace everblu_cyble_enhanced {

static const char *const TAG = "everblu_cyble_enhanced.sensor";

void EverbluCybleEnhancedComponent::setup() {
    ESP_LOGD(TAG, "Init connection with a frequency of %f MHz", this->frequency_);
    cc1101_init(this->frequency_);
}

void EverbluCybleEnhancedComponent::dump_config() {
    ESP_LOGCONFIG(TAG, "Dump config...");
}

float EverbluCybleEnhancedComponent::get_setup_priority() const { return setup_priority::DATA; }

void EverbluCybleEnhancedComponent::update() {
    struct tmeter_data meter_data;
    ESP_LOGV(TAG, "Meter year: %d | Meter serial: %d", this->year_, this->serial_);
    meter_data = get_meter_data(this->year_, this->serial_);
    ESP_LOGD(TAG, "Index: %d | Battery: %d monthes | Reads: %d | Online start hour: %d | Online end hour: %d", meter_data.liters, meter_data.battery_left, meter_data.reads_counter, meter_data.time_start, meter_data.time_end);

    if (this->index_sensor_ != nullptr && meter_data.liters > 0)
        this->index_sensor_->publish_state(meter_data.liters);
}

}  // namespace everblu_cyble_enhanced
}  // namespace esphome