# ESPHome devices

## Flash using USB
```bash
docker run --rm --device /dev/ttyUSB0:/dev/ttyUSB0 -v $PWD:/config -it esphome/esphome run <config.yaml>
```


## Flash using OTA
```bash
docker run --rm -v $PWD:/config -it esphome/esphome run <config.yaml>
```

## View logs
```bash
docker run --rm -v $PWD:/config -it esphome/esphome logs <config.yaml>
```